{-# LANGUAGE OverloadedStrings #-}

module Control.Monad.Trans.Gender
    (
        transgender
    ) where

import Rainbow
import Data.Function ((&))

transgender = do
    putChunkLn $ "=======================" & fore blue
    putChunkLn $ "=======================" & fore brightMagenta -- pink
    putChunkLn $ "=======================" & fore white
    putChunkLn $ "=======================" & fore brightMagenta -- pink again
    putChunkLn $ "=======================" & fore blue
